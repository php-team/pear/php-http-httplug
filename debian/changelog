php-http-httplug (2.4.1-1) unstable; urgency=medium

  [ David Buchmann ]
  * prepare release

  [ Alexander M. Turek ]
  * Make nullable parameter types explicit

  [ David Prévot ]
  * Update Standards-Version to 4.7.0
  * Revert "Force system dependencies loading"

 -- David Prévot <taffit@debian.org>  Mon, 30 Sep 2024 13:56:51 +0200

php-http-httplug (2.4.0-3) unstable; urgency=medium

  * Force system dependencies loading

 -- David Prévot <taffit@debian.org>  Wed, 06 Mar 2024 15:40:02 +0100

php-http-httplug (2.4.0-2) unstable; urgency=medium

  * Upload to unstable now that Bookworm has been released

 -- David Prévot <taffit@debian.org>  Wed, 03 Jan 2024 08:36:14 +0100

php-http-httplug (2.4.0-1) experimental; urgency=medium

  * Upload new minor to experimental

  [ Nicolas Grekas ]
  * Deprecate Http\Client\HttpClient and allow psr/http-message v2

  [ David Buchmann ]
  * prepare release

  [ David Prévot ]
  * Update standards version to 4.6.2, no changes needed.
  * Drop debian/pkg-php-tools-* in favor of updated build-dependencies

 -- David Prévot <taffit@debian.org>  Sat, 15 Apr 2023 08:32:00 +0200

php-http-httplug (2.3.0-2) unstable; urgency=medium

  * Mark package as Multi-Arch: foreign
  * Update debian/watch URL

 -- David Prévot <taffit@debian.org>  Tue, 20 Sep 2022 13:25:06 +0200

php-http-httplug (2.3.0-1) unstable; urgency=medium

  * Fix d/watch after hosting change
  * New upstream version 2.3.0
  * Use dh-sequence-phpcomposer instead of --with phpcomposer
  * Generate phpabtpl at build time
  * Update Standards-Version to 4.6.1
  * Update watch file format version to 4.

 -- David Prévot <taffit@debian.org>  Mon, 20 Jun 2022 15:44:49 +0200

php-http-httplug (2.2.0-1) unstable; urgency=medium

  [ Graham Campbell ]
  * Support PHP 7.1-8.0
  * Release 2.2.0

  [ David Prévot ]
  * Use secure URI in Homepage field.
  * Update standards version to 4.5.0, no changes needed.
  * Set Rules-Requires-Root: no.
  * Drop versioned dependency satisfied in (old)stable
  * Use debhelper-compat 13

 -- David Prévot <taffit@debian.org>  Thu, 16 Jul 2020 06:53:46 -0400

php-http-httplug (2.1.0-1) unstable; urgency=medium

  [ David Buchmann ]
  * prepare release

  [ David Grayston ]
  * PSR-18: Network / Request exception inheritance (#158)

 -- David Prévot <taffit@debian.org>  Thu, 02 Jan 2020 08:47:49 +1100

php-http-httplug (2.0.0-2) unstable; urgency=medium

  * Set upstream metadata fields:
    Bug-Database, Repository, Repository-Browse, Bug-Submit
  * Update standards version to 4.4.1, no changes needed.

 -- David Prévot <taffit@debian.org>  Fri, 27 Dec 2019 09:27:13 +1100

php-http-httplug (2.0.0-1) unstable; urgency=medium

  * Initial release (new symfony build-dependency)

 -- David Prévot <taffit@debian.org>  Mon, 01 Jul 2019 15:01:02 -1000
